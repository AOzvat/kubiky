#ifndef PAINTWIDGET_H
#define PAINTWIDGET_H

#include <QColor>
#include <QImage>
#include <QPoint>
#include <QWidget>
#include <QtWidgets>
#include <cmath>
#include <iostream>
#include <QLineEdit>
#include <algorithm>

class PaintWidget : public QWidget
{
	Q_OBJECT

public:
	PaintWidget(QWidget *parent = 0);

	bool openImage(const QString &fileName);
	bool newImage(int x, int y);
	bool saveImage(const QString &fileName);
	void setPenColor(const QColor &newColor);
	void setPenWidth(int newWidth);

	bool isModified() const { return modified; }
	QColor penColor() const { return myPenColor; }
	int penWidth() const { return myPenWidth; }

	public slots:
	void clearImage();
	void nastav_farbu_hranici(int red = 0, int green = 0, int blue = 0, int a = 255);
	void nastav_farbu_vyplne(int red = 255, int green = 0, int blue = 0, int a = 255);
	void dda();
	void bresen();
	void kresli_body(int pocet_body, double polomer = 100);

	void posun();
	void otoc(float hodota, bool smer);
	void skaluj(float hodnota);
	void preklop_ciara();
	void preklop();
	void skos(float hodnota, bool smer);
	void vymaz_vsetko();

	void scan_line();

	void bezierova_krivka();
	void coonsova_kubika();

	float vypocet_koeficientov0(float t);
	float vypocet_koeficientov1(float t);
	float vypocet_koeficientov2(float t);
	float vypocet_koeficientov3(float t);

	void dda_point(QPoint prvy, QPoint druhy);

protected:
	void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
	void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
	void mouseDoubleClickEvent(QMouseEvent *event);

private:
	void drawLineTo(const QPoint &endPoint);
	void resizeImage(QImage *image, const QSize &newSize);

	bool modified;
	bool painting;
	int myPenWidth;
	QColor myPenColor;
	QImage image;
	QPoint lastPoint;
	QPoint lastPoint2;
	QColor farba_hranici;
	QColor farba_vyplne;
	std::vector<QPoint> body;
	bool posunutie = false;
	std::vector<QPoint> body_posunu;
};

#endif // PAINTWIDGET_H