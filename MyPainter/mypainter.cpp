#include "mypainter.h"

MyPainter::MyPainter(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.scrollArea->setWidget(&this->paintWidget);
	ui.scrollArea->setBackgroundRole(QPalette::Dark);
	paintWidget.newImage(800, 600);
}

MyPainter::~MyPainter()
{

}

void MyPainter::ActionOpen()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", "image files (*.png *.jpg *.bmp)");
	if (!fileName.isEmpty())
		paintWidget.openImage(fileName);
}

void MyPainter::ActionSave()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), "untitled.png", tr("png Files (*.png)"));
	if (fileName.isEmpty()) {
		return;
	}
	else {
		paintWidget.saveImage(fileName);
	}
}

void MyPainter::EffectClear()
{
	paintWidget.vymaz_vsetko();
}

void MyPainter::ActionNew()
{
	paintWidget.newImage(800, 600);
}

void MyPainter::Help() {
	//sprava pre zaciatok
	QMessageBox box;
	box.setText("NAVOD: \n\nMoznost Polygony: \n Body polygonu zadavajte lavim tlacidlom. \n Zadavanie bodou ukoncite lavim dvojklikom. \n Stlacenim praveho tlacidla sa polygon vyfarby. \n\nNastavenie Posun: \n Pre posun najprv zadajte zaciatocny bod posunu a nasledne bod, kam chcete objekt posunut. \n\nMoznost Bezierova krivka alebo Coonsova kubuka: \n Body zadavajte lavim tlacidlom. \n\nKazda operacia, ktoru chcete vykonat sa vykona po stlaceni tlacidla Kresli. \n\nTlacidlom Vymaz, vymazete body na platne, NIE nastavenia.");
	box.exec();
}

void MyPainter::KresliClicked()
{
	float hodnota = 0.0;

	//vymaze kresliacu plochu
	paintWidget.clearImage();

	//nastavi zadanu farbu hranici
	nastavi_farbu_hranici();

	//nastavi zadanu farbu vyplne
	nastavi_farbu_vyplne();

	//vykresli povodny objekt
	//ALE NETREBA TO, je to na kontrolu
//	paintWidget.dda();

	//kruznica alebo polygon alebo Bezierova krivka alebo Coonsova kubika
	//kruznica
	if (ui.comboBox_2->currentIndex() == 1) {
		//vykresli body kruznice
		vykresli_body();

		//dda
		if (ui.comboBox_3->currentIndex() == 0) {
			paintWidget.dda();
		}

		//Bresenham
		if (ui.comboBox_3->currentIndex() == 1) {
			paintWidget.bresen();
		}

		//vyplni oblast
		paintWidget.scan_line();
	}

	//polygon
	if (ui.comboBox_2->currentIndex() == 0) {
		//posuvanie
		if (ui.comboBox->currentIndex() == 0) {
			paintWidget.posun();
		}

		//otacanie
		if (ui.comboBox->currentIndex() == 1) {
			//nacita hodnotu uhla
			if (ui.lineEdit->text() == "") {
				hodnota = 30.0;
			}
			else {
				hodnota = ui.lineEdit->text().toFloat();
			}

			//v smere hodinovych rucuciek
			if (ui.radioButton->isChecked()) {
				paintWidget.otoc(hodnota, true);
			}

			//proti smeru hodinovych ruciciek
			if (ui.radioButton_2->isChecked()) {
				paintWidget.otoc(hodnota, false);
			}
		}

		//skalovanie
		if (ui.comboBox->currentIndex() == 2) {
			//nacita hodnotu koeficienta
			if (ui.lineEdit->text() == "") {
				hodnota = 2.0;
			}
			else {
				hodnota = ui.lineEdit->text().toFloat();
			}

			paintWidget.skaluj(hodnota);
		}

		//preklopenie
		if (ui.comboBox->currentIndex() == 3) {
			paintWidget.preklop_ciara();
			paintWidget.preklop();
		}

		//skosenie
		if (ui.comboBox->currentIndex() == 4) {
			//nacita hodnotu koeficienta
			if (ui.lineEdit->text() == "") {
				hodnota = 1.0;
			}
			else {
				hodnota = ui.lineEdit->text().toFloat();
			}

			//v smere osi x
			if (ui.radioButton_3->isChecked()) {
				paintWidget.skos(hodnota, true);
			}

			//v smere osi y
			if (ui.radioButton_4->isChecked()) {
				paintWidget.skos(hodnota, false);
			}
		}

		//vyplni oblast
		paintWidget.scan_line();
	}

	//Bezierova krivka
	if (ui.comboBox_2->currentIndex() == 2) {
		paintWidget.bezierova_krivka();
	}

	//Coonsova kubika
	if (ui.comboBox_2->currentIndex() == 3) {
		paintWidget.coonsova_kubika();
	}
}

void MyPainter::vykresli_body() {
	if (ui.lineEdit_8->text() == "") {
		paintWidget.kresli_body(ui.spinBox->value());
	}
	else {
		paintWidget.kresli_body(ui.spinBox->value(), ui.lineEdit_8->text().toDouble());
	}
}

void MyPainter::nastavi_farbu_hranici() {
	if (ui.lineEdit_3->text() == "" && ui.lineEdit_4->text() == "" && ui.lineEdit_5->text() == "") {
		paintWidget.nastav_farbu_hranici();
	}
	else {
		paintWidget.nastav_farbu_hranici(ui.lineEdit_3->text().toInt(), ui.lineEdit_4->text().toInt(), ui.lineEdit_5->text().toInt());
	}
}

void MyPainter::nastavi_farbu_vyplne() {
	if (ui.lineEdit_7->text() == "" && ui.lineEdit_2->text() == "" && ui.lineEdit_6->text() == "") {
		paintWidget.nastav_farbu_vyplne();
	}
	else {
		paintWidget.nastav_farbu_vyplne(ui.lineEdit_7->text().toInt(), ui.lineEdit_2->text().toInt(), ui.lineEdit_6->text().toInt());
	}
}